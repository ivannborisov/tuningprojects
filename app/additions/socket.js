import io from 'socket.io-client'

let socket = io('http://localhost:9000');
console.log('Connecting socket to server......');

socket.emit('fromclient', {qko: 'test'});
socket.on('news', function(data){
    console.log(data);
});

export default  socket;