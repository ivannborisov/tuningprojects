'use strict';
import React from 'react';
import $ from 'jquery';
import { Router, Route,  IndexRoute   } from 'react-router';
import {browserHistory}  from 'react-router';
import About from './views/about';
import CreateProject from './views/projects/create';
import {Logout, Register} from './views/auth/auth';
import VerifyAccount from './views/auth/verify-account';
import AuthService from './services/auth.service';
import TitleService from './services/title.service';
import MainLayout from './views/main-layout';
import Home from './views/home';
import Projects from './views/projects';
import SingleProject from './views/projects/single-project';

class App extends React.Component {

    constructor(props){
        super(props);
        this.authServiceSingleton = new AuthService();
        this.titleServiceSingleton = new TitleService();
        this.requireAuth = this.requireAuth.bind(this);
        this.state = {
            ready: false
        };

    }
    componentWillMount() {

        $.ajax({
            method: 'POST',
            url: '/api/checklogged',
            dataType: 'json',
            cache: false
        }).done((res) => {

            this.authServiceSingleton.setUser(res);

            this.setState({
                 ready:true
             });

        }).fail(() => {
            this.authServiceSingleton.setUser({success:false});

            this.setState({
                ready:true
            });
        });


    }
    getChildContext () {

        return {
            authService: this.authServiceSingleton,
            titleService: this.titleServiceSingleton
        }
    }

    requireAuth(nextState, replace) {
        if (!this.authServiceSingleton.isLogged()) {
            replace({
                pathname: '/',
                state: { nextPathname: nextState.location.pathname }
            })
        }
    }
    render () {
        if(this.state.ready) {

          //  console.log(this.authServiceSingleton.getUser());
            return (
                <Router history={browserHistory}>
                    <Route path="/" component={MainLayout}>

                        <IndexRoute component={Home}/>
                        <Route path="/about" component={About} />
                        <Route path="/projects" component={Projects}>
                            <Route path="/projects/:section" component={Projects} />
                        </Route>
                        <Route path="/project/:id" component={SingleProject} />
                        <Route path="/accounts/activate/:code" component={VerifyAccount} sendnewcode={false}/>
                        <Route path="/accounts/activate" component={VerifyAccount} sendnewcode={true}/>
                        {/*<Route path="/projects/:section" component={Projects} />*/}
                        {/*<Route path="/projects/motorbikes" component={Projects} />*/}
                        <Route path="/createproject" component={CreateProject}  onEnter={this.requireAuth} />
                        <Route path="/logout" component={Logout}/>
                        <Route path="/register" component={Register}/>
                    </Route>
                </Router>
            );
        }
        else {
            return (<div>Loading</div>)
        }
    }
}

App.childContextTypes = {
    authService: React.PropTypes.object,
    titleService: React.PropTypes.object
};

export default App;