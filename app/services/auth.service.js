"use strict";
import $ from 'jquery';

class AuthService {


    constructor() {
        this.currentUser = undefined;
    }

    getUser() {
        return this.currentUser;
    }

    setUser(user) {
        if(user.success)
            this.currentUser = user.user;
        else
            this.currentUser = undefined;
    }
    getUsername(){
        if(this.currentUser)
            return this.currentUser.username;
        return undefined;
    }
    getUserId() {
        if(this.currentUser)
            return this.currentUser._id;
        return undefined;
    }
    logOut() {
        this.currentUser = undefined;
    }
    isLogged() {
        return !!this.currentUser;
    }

    checkForLoggedUser(cb) {
        $.ajax({
            method: 'POST',
            url: '/api/checklogged',
            dataType: 'json',
            cache: false
        }).done((res) => {

            this.setUser(res);
            cb();
        }).fail(() => {

            this.setUser({success:false});
            cb();
        });
    }


}

export default AuthService;