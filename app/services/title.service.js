"use strict";
class TitleService {


    constructor() {
        document.title = 'Tuning Projects';
    }
    
    setTitle(title) {
        document.title = title;
    }

}

export default TitleService;