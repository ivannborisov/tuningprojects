import React from 'react';
import axios from 'axios';
import socket from '../../additions/socket';

class Comment extends React.Component {

    constructor(props, context) {
        super(props, context);
        this.state = {
            commentContent: '',
            comments: this.props.comments
        };
        this.handleChangeTextarea = this.handleChangeTextarea.bind(this);
        this.sendComment = this.sendComment.bind(this);
    }

    handleChangeTextarea(e) {
        this.setState({commentContent: e.target.value});
    }

    componentWillMount(){
        socket.on('newcomment', (data) => {

            if(data.comment.toId === this.props.id ) {
                let newCommentsArr = this.state.comments;
                newCommentsArr.push(data.comment);
                this.setState({comments: newCommentsArr});
            }
        });
    }

    sendComment() {
        let commentContent = this.state.commentContent.trim();
        if(commentContent !== '') {
            axios.post('/api/comments', {
                toModel: this.props.to,
                toId: this.props.id,
                content: commentContent,
                user: this.context.authService.getUserId(),
                project: this.props.projectId
            })
            .then(function (response) {
                console.log('Created' + response);
            })
            .catch(function (error) {
                console.log(error); //TO DO
            });
            this.setState({commentContent: ''});
        }
    }

    render () {

        let commentsNodes = this.state.comments.map((comment) => {
            return (<div className="project-comment" key={comment._id}><p className="project-comment-date">{comment.created}</p><p>{comment.content}</p></div>);
        });


        return(<div>
                    <div>{commentsNodes}</div>
                    {this.context.authService.isLogged() ?
                        <div>
                            <textarea onChange={this.handleChangeTextarea}/>
                            <button onClick={this.sendComment}>Comment {this.props.to}</button>
                        </div>
                        :
                        false
                    }
               </div>);
    }
}

Comment.propTypes = {
    to: React.PropTypes.string,
    id: React.PropTypes.string,
    comments: React.PropTypes.array,
    projectId: React.PropTypes.string
};

Comment.contextTypes = {
    authService: React.PropTypes.object
};

export default Comment;