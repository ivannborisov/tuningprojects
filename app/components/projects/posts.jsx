import React from 'react';
import Comment from '../../components/projects/comment';
import socket from '../../additions/socket';

class Posts extends React.Component {

    constructor (props) {
        super(props);
        this.state = {
            posts : this.props.posts
        }
    }

    componentWillMount () {
        socket.on('newpost', (data) => {

            if(data.post.project === this.props.projectId && data.date === this.props.date ) {
                let newPostsArr = this.state.posts.slice();
                newPostsArr.unshift(data.post);
                this.setState({posts: newPostsArr});
            }

        });
    }

    render () {
        let postsNodes = this.state.posts.map((post) => {
            return (<div key={post._id} className="project-post"> {(new Date(post.created)).getHours() + ':' + (new Date(post.created)).getMinutes()} <hr className="thin"/> <p> {post.content} </p>   <Comment projectId={this.props.projectId} to="post" comments={post.comments} id={post._id}/> </div>);
        });
        return (<div>{postsNodes}</div>)
    }

}

Posts.propTypes  = {
    date: React.PropTypes.string,
    posts: React.PropTypes.array,
    projectId: React.PropTypes.string
};

export default Posts;