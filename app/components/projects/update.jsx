import React from 'react';
import UploadPhotos from  './upload-photos';
import axios from 'axios';

class UpdateProject extends  React.Component {

    constructor(props, context) {
        super(props, context);
        this.state = {
          content: ""
        };
        this.handleUpdate = this.handleUpdate.bind(this);
        this.handleChangeTextarea = this.handleChangeTextarea.bind(this);

    }

    handleUpdate(){

        let getProjectFunc =  this.props.getProjectFunc;
        if(this.state.content !== ""){
            axios.post('/api/updates', {
                projectid: this.props.id,
                content: this.state.content
            })
            .then(function (response) {
                console.log(response);
            })
            .catch(function (error) {
                console.log(error); //TO DO
            });
        }

        this.refs.uploadphotos.uploadPhotos(getProjectFunc);
        this.setState({content: ''});
    }
    handleChangeTextarea(e) {
        this.setState({content: e.target.value});
    }

    render() {

        return (
            <div>
                <textarea onChange={this.handleChangeTextarea}/>
                <div>
                    Upload photos: <UploadPhotos ref="uploadphotos"  projectId={this.props.id} />
                </div>
                <button onClick={this.handleUpdate}>Update project</button>
            </div>
        );
    }

}

UpdateProject.propTypes = {
    id: React.PropTypes.string,
    getProjectFunc: React.PropTypes.func

};

export default UpdateProject;
