import React from 'react';

class UploadPhotos extends  React.Component {

    constructor(props, context) {
        super(props, context);
    }

    uploadPhotos(cb) {

        let filesSelected = document.getElementById('selectedFiles');
        let files = filesSelected.files;

        if(files.length !== 0 ) {
            let countImages = 0;
            let formData = new FormData();
            for (let i = 0; i < files.length; i++) {
                let file = files[i];
                // Check the file type.
                if (!file.type.match('image.*')) {

                    continue;
                }
                ++countImages;
                formData.append('photos', file, file.name);
            }
            if(countImages) {

                formData.append('projectid', this.props.projectId);
                let xhr = new XMLHttpRequest();
                xhr.open('POST', '/api/uploadphotos', true);

                xhr.onload = () => {
                    if (xhr.status === 200) {

                        cb();
                        console.log('Uploaded photos');
                    } else {
                        alert('An error occurred!');
                    }
                };
                xhr.send(formData);
            }

        }
        else {
            cb();
        }

    }

    render() {
        return (<input type="file" name="photos" id="selectedFiles" multiple="multiple"/>);
    }

}
UploadPhotos.propTypes = {
    projectId: React.PropTypes.string
};

export default UploadPhotos;
