import React from 'react';
import Comment from '../../components/projects/comment';
import socket from '../../additions/socket';

class Photos extends React.Component {

    constructor(props){
        super(props);
        this.state = {
            photos : this.props.photos
        }
    }

    componentWillMount () {
        socket.on('newphotos', (data) => {
            console.log('new photos');
            console.log(data);
            console.log(this.props.projectId);
            console.log(this.props.date);
            if(data.projectId == this.props.projectId && data.date == this.props.date ) {
                console.log('new photos 2');
                let newPhotosArr = data.photos.concat(this.state.photos);
                this.setState({photos: newPhotosArr});

            }

        });
    }

    render () {
         let photosNodes = this.state.photos.map((photo) => {
            return (<div key={photo._id} className="project-photo"><img src={"/public/images/uploads/" + photo.name}  />   <Comment projectId={this.props.projectId} to="photo" comments={photo.comments} id={photo._id}/> </div>);
         });

        return (<div>{photosNodes}</div>)
    }



}

Photos.propTypes  = {
    date: React.PropTypes.string,
    photos: React.PropTypes.array,
    projectId: React.PropTypes.string
};

export default Photos;