import React from 'react';
import $ from 'jquery';
import NavLink from '../../components/nav/nav-link';

class Project extends  React.Component {
    constructor(props) {
        super(props);
    }
    render() {

        let imagePath = "/public/images/uploads/" + this.props.img;

        return (
            <div>
                <p><NavLink  to={'/project/' + this.props.id }>{this.props.name}</NavLink> </p> <img src={imagePath}  />
            </div>
        )

    }

}

Project.propTypes = {
    name: React.PropTypes.string,
    img: React.PropTypes.string,
    id: React.PropTypes.string
};



class LastProjects extends React.Component {

    constructor (props) {
        super(props);

        this.state = {
            section: this.props.section,
            projects: []
        }
    }

    getProjects (currSection) {
        $.get('/api/projects', {section: currSection},  (data) => {
            this.setState({projects: data});
        });
    }

    componentWillMount () {
        this.getProjects(this.state.section);
    }

    componentWillReceiveProps (nextProps) {
        this.getProjects(nextProps.section);
    }

    render () {
        let projectsNodes = [];
        this.state.projects.forEach((project) => {
           projectsNodes.push(<Project name={project.title} img={project.mainImg} id={project._id} key={project._id}/> );
        });

        return (

           <div className="last-projects-wrapper">
               {projectsNodes}
           </div>

        )
    }

}

LastProjects.propTypes = {
    params: React.PropTypes.object,
    section: React.PropTypes.string
};

export default LastProjects;
