import React from 'react';
import Photos from '../../components/projects/photos';
import Posts from '../../components/projects/posts';
import socket from '../../additions/socket';

class Updates extends React.Component {

    constructor (props) {
        super(props);
        this.state = {
            updates: this.props.updates
        }
    }

    componentWillMount () {
        socket.on('newpost', (data) => {

            if (this.props.projectId === data.post.project && (this.state.updates.length === 0 || data.date !== this.state.updates[0].date)) {
                let newUpdatesArr = this.state.updates.slice();
                let newUpdate = {
                    date: data.date,
                    posts: [],
                    photos:[]
                };
                newUpdate.posts.push(data.post);
                newUpdatesArr.unshift(newUpdate);
                this.setState({updates: newUpdatesArr});
            }
        });

        socket.on('newphotos', (data) => {

            if (this.props.projectId === data.projectId && (this.state.updates.length === 0 || data.date !== this.state.updates[0].date)) {
                let newUpdatesArr = this.state.updates.slice();
                let newUpdate = {
                    date: data.date,
                    posts: [],
                    photos: data.photos.slice()
                };
                newUpdatesArr.unshift(newUpdate);
                this.setState({updates: newUpdatesArr});
            }
        });

    }

    render () {

        let updateNodes = [];
        let updates = this.state.updates;

        for (let update of updates)
            updateNodes.push(<div className="project-update" key={update.date}><hr className="wide"/> <p className="project-date">{update.date}</p> <hr className="wide"/>  <Posts projectId={this.props.projectId} posts={update.posts} date={update.date} />  <Photos projectId={this.props.projectId} date={update.date} photos={update.photos} />  </div>);

        return (<div> {updateNodes}</div>);
    }
}

Updates.propTypes = {
  updates: React.PropTypes.array,
  projectId: React.PropTypes.string
};


export default Updates;