import React from 'react';
import axios from 'axios';
import socket from '../additions/socket';

class LastActivities extends  React.Component {

    constructor (props) {
        super(props);
        this.state = {
            lastActivities: []
        };
    }


    getLastActivities() {
        axios.get('/api/lastactivities')
            .then((response) => {
               this.setState({
                   lastActivities : response.data
               });
                console.log(this.state.lastActivities);
            })
            .catch(function (error) {
                console.log(error); //TO DO
            });
    }
    componentWillMount() {
        this.getLastActivities();
    }
    componentDidMount() {

        // console.log('LAST ACTIVITIES');
        // socket.on('newactivity', (data) => {
        //     console.log('Activityyy');
        //     let lastActivitiesArr = this.state.lastActivities.slice();
        //     lastActivitiesArr.unshift(data.activity);
        //     this.setState({lastActivities: lastActivitiesArr});
        // });

        socket.on('newcomment', (data) => {
            let activity = data.comment;
            activity.type = 'comment';
            let lastActivitiesArr = this.state.lastActivities.slice();
            console.log(activity);
            lastActivitiesArr.unshift(activity);
            this.setState({lastActivities: lastActivitiesArr});
        });
        socket.on('newphoto', () => {

        });
        socket.on('newpost', () => {

        });

    }
    render () {

        let activities = [];


        this.state.lastActivities.forEach((act) => {

            activities.push(<div key={act.id}>{act.type}</div>);
        });


        return (
            <div className="last-activities">
                <h4>Last Activities</h4>
                {activities}
            </div>

        );

    }
}


export default LastActivities;