"use strict";
import React from 'react';
import NavLink from '../components/nav/nav-link';
import Home from './home';
import {Login} from './auth/auth';

class MainLayout extends React.Component {
    constructor(props, context) {
        super(props, context);
        context.titleService.setTitle('Tuning Projects');
    }

    render() {
            return (
                <div>
                    <header>
                        <div className="container">
                            <div className="col-2">
                                <h1>Tuning Projects</h1>
                                <nav>
                                    <ul role="nav">
                                        <li><NavLink onlyActiveOnIndex={true} to="/">Home</NavLink></li>
                                        <li><NavLink to="/projects/cars">Cars</NavLink></li>
                                        <li><NavLink to="/projects/motorbikes">Motorbikes</NavLink></li>
                                        <li><NavLink to="/about">About</NavLink></li>

                                    </ul>
                                </nav>
                            </div>
                            <div className="col-2">
                                {this.context.authService.isLogged() ?
                                    <div className="logged-user-nav">
                                        <NavLink to="/createproject">Create project</NavLink>
                                        <NavLink to="/logout">Logout</NavLink>
                                    </div>
                                    :
                                    <Login />

                                }
                            </div>

                        </div>
                    </header>

                    {this.props.children || <Home /> }


                    <div className="wrapper">
                        <footer>
                            <div className="container">
                                <h6>Tuningprojects-bg.com</h6>
                            </div>
                        </footer>

                    </div>
                </div>
            );
    }

}

MainLayout.propTypes = {
    children: React.PropTypes.node,
};

MainLayout.contextTypes = {
    authService: React.PropTypes.object,
    titleService: React.PropTypes.object
};


export default MainLayout;