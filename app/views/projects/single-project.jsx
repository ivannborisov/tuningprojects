import React from 'react';
import axios from 'axios';
import UpdateProject from '../../components/projects/update';
import Updates from '../../components/projects/updates';

class SingleProject extends  React.Component {

    constructor(props, context){
        super(props, context);
        this.context.titleService.setTitle('Projects');

        this.state = {
            projectId: this.props.params.id,
            project: undefined,
            isCurrUserOwner: false
        };
        this.getProject = this.getProject.bind(this);



    }


    // genProjectUpdates() {
    //     let updatesArr = this.state.project.updates;
    //     let photosArr = this.state.project.photos;
    //     // console.log('HERE');
    //     // console.log(photosArr[0].created.toString().substr(0,10));
    //
    //
    //     let updatesSortedByDate = {};
    //     for(let key in photosArr) {
    //         let photo = photosArr[key];
    //         let photoCreateDate = photo.created.toString().substr(0,10)
    //
    //         if(updatesSortedByDate[photoCreateDate])
    //             updatesSortedByDate[photoCreateDate].push(photo);
    //         else {
    //             updatesSortedByDate[photoCreateDate] = [];
    //             updatesSortedByDate[photoCreateDate].push(photo);
    //         }
    //     }
    //
    //     for(let key in updatesArr) {
    //         let update = updatesArr[key];
    //         let updateCreateDate = update.created.toString().substr(0,10)
    //
    //         if(updatesSortedByDate[updateCreateDate])
    //             updatesSortedByDate[updateCreateDate].push(update);
    //         else {
    //             updatesSortedByDate[updateCreateDate] = [];
    //             updatesSortedByDate[updateCreateDate].push(update);
    //         }
    //     }
    //
    //     console.log(updatesSortedByDate);
    //
    // }

    getProject() {
        return axios.get('/api/projects/' + this.state.projectId)
            .then((response) => {
                let project = response.data;
                let isOwner = false;
                if(project.owner === this.context.authService.getUsername())
                    isOwner = true;

                this.setState({project: project, isCurrUserOwner: isOwner});
                //this.genProjectUpdates();
            })
            .catch((error) => {
                console.log(error);
            });

    }

    componentWillReceiveProps(nextProps) {

        this.setState({projectId: nextProps.params.id});
    }
    componentWillMount() {


        this.getProject();
    }

    render() {


        if(this.state.project) {

            return (
                <div className="container content">
                    <div className="row">
                        <h2>{this.state.project.title}</h2>
                        <p>Owner - {this.state.project.owner}</p>
                        <div>
                            <Updates projectId={this.state.projectId} updates={this.state.project.updates} />
                        </div>
                    </div>
                    {this.state.isCurrUserOwner ?

                        <div className="row">
                            <hr className="wide" />
                            <h2>Update your project</h2>
                            <hr className="wide" />
                            <UpdateProject getProjectFunc={this.getProject} id={this.state.projectId} />
                        </div>

                        :
                        false
                    }
                </div>

            );
        }
        else
            return false;

    }

}


SingleProject.propTypes = {
    params: React.PropTypes.object,
    id: React.PropTypes.string
};

SingleProject.contextTypes = {
    authService: React.PropTypes.object,
    titleService: React.PropTypes.object
};

export default SingleProject;