import React from 'react';

class CreateProject extends React.Component {

    constructor(props){
        super(props);
        this.state = {title: '', section: ''};
        this.setState = this.setState.bind(this);
        this.handleCreateProject = this.handleCreateProject.bind(this);
        this.handleTitle = this.handleTitle.bind(this);
        this.handleSection = this.handleSection.bind(this);

    }

    handleCreateProject(e){
        e.preventDefault();

        let fileSelect = document.getElementById('selectedFile');
        let file = fileSelect.files;
        let formData = new FormData();

        if(fileSelect.files.length !== 0 ) {
            if (file[0].type.match('image.*')) {
                formData.append('photo', file[0], file[0].name);
            }
        }
        console.log(this.context.authService.getUser());
        formData.append('owner', this.context.authService.getUser().username);
        formData.append('title', this.state.title);
        formData.append('section', this.state.section);
        let xhr = new XMLHttpRequest();
        xhr.open('POST', '/api/projects', true);

        xhr.onload = () => {
            if (xhr.status === 200) {

                this.context.router.push('/'); // TO DO - path to created project
            } else {
                alert('An error occurred!');
            }
        };

        xhr.send(formData);
            
    }
    
    handleTitle(e){
        this.setState({title: e.target.value});
    }
    handleSection(e){
        console.log(e.target.value);
        this.setState({section: e.target.value});
    }
    
    render(){
        return (

            <div className="container content">
                <div className="row">
                    <h2>Create project</h2>
                        <div>
                            <input type="text" value={this.state.title}  placeholder="Title" onChange={this.handleTitle} />
                        </div>
                        <div className="col-2">

                            <select onChange={this.handleSection}>
                                <option>Choose section</option>
                                <option value="cars">Cars</option>
                                <option value="motorbikes">Motorbikes</option>
                            </select>

                        </div>
                        <div className="col-2">
                            <p>Choose main photo of project:</p>
                            <input type="file" name="photo" id="selectedFile" />
                        </div>
                        <div>
                            <button onClick={this.handleCreateProject}>
                                Create project
                            </button>
                        </div>
                </div>
            </div>
        );
    }
}

CreateProject.contextTypes =  {
    authService: React.PropTypes.object,
    router: React.PropTypes.object
};

export default CreateProject;