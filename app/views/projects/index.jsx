import React from 'react';
import LastProjects from '../../components/projects/last-projects';

class Projects extends React.Component {

    constructor(props, context){
        super(props, context);
        this.context.titleService.setTitle('Projects');

        this.state = {
            section: this.props.params.section
        }

    }

    componentWillReceiveProps(nextProps) {

        this.setState({section: nextProps.params.section});
    }

    render(){
        return (



            <div className="container content">
                <div className="row">
                    <h1>Project - {this.state.section}</h1>
                    <LastProjects section={this.state.section} />
                </div>
            </div>
        );
    }

}


Projects.propTypes = {
    params: React.PropTypes.object
};

Projects.contextTypes = {
    titleService: React.PropTypes.object
};

export default Projects;