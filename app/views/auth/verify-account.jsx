import React from 'react';
import axios from 'axios';

class VerifyAccount extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            code: this.props.params.code,
            success: undefined,
            email: ''
        };


        this.handleChangeEmail = this.handleChangeEmail.bind(this);


    }

    componentWillMount() {

        console.log(this.props.route.sendnewcode);
        if(this.props.route.sendnewcode) {
            this.sendNewActivationCodeToEmail();
            console.log('Sending new code');
        }
        else {

            this.sendCode();
        }
    }


    sendNewActivationCodeToEmail() {

        axios.post('/api/verifyemail/', {
            email: this.state.email
        })
        .then((response) => {
            console.log(response);
        })
        .catch( (error) => {
            console.log(error); //TO DO
        });

    }
    sendCode() {
        console.log('Sending....')
        axios.get('/api/verifyemail/' + this.state.code)
        .then( () => {
            console.log('Sended')

            this.setState({success: true});
        })
        .catch(() => {
            console.log('Erorr')
            this.setState({success: false}); //TO DO
        });
    }
    handleChangeEmail(e) {
        this.setState({email: e.target.value});
        console.log(this.state.email);
    }



    render () {

        if(this.props.route.sendnewcode)
            return (<div><input  type="text" value={this.state.email}  placeholder="Email address" onChange={this.handleChangeEmail} /> <button onClick={this.sendActivationCode}>Get activation code</button> </div>);
        if(this.state.success === undefined)
            return null;
        if(this.state.success)
            return (<div className="container content"><div className="row"><h3>Successful activation</h3></div></div>);
        else
            return (<div className="container content"><div className="row"><h3>Invalid activation code!</h3></div></div>);


    }


}

VerifyAccount.propTypes = {
    params: React.PropTypes.object,
    code: React.PropTypes.string,
    route: React.PropTypes.object,
    sendnewcode: React.PropTypes.bool
};

export default VerifyAccount;