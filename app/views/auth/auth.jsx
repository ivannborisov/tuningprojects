'use strict';
import React from 'react';
import LoginForm from './login-form';
import $ from 'jquery';
import RegisterForm from './register-form';

var Router = require('react-router');

class Login extends React.Component{
    constructor(props, context) {
        super(props, context);

        this.handleLoginSubmit = this.handleLoginSubmit.bind(this);

    }
    handleLoginSubmit(user){

        let url='/api/login';
        
        $.ajax({
            method: 'POST',
            url: url,
            dataType: 'json',
            data: user,
            cache: false
        }).done((res) => {
            this.context.authService.setUser(res);
            Router.browserHistory.push('/');
        }).fail((xhr) => {
           let res = xhr.responseJSON;
            if(res.reason === 'email_not_verified')
                alert('Your email is not verified! Please check your email.');
            else
                alert('Invalid username or password');
        });

    }

    render(){
        return (
            <div className='login'>
                <LoginForm  onLoginSubmit={this.handleLoginSubmit}/>
            </div>
        );
    }
}

class Logout extends React.Component{
    constructor(props, context) {
        super(props, context);

    }
    componentDidMount() {

        $.ajax({
            method: 'GET',
            url: '/api/logout',
            cache: false
        }).done(() => {
            this.context.authService.logOut();
            Router.browserHistory.push('/');
        }).fail((xhr, status, err) => {
            console.error('/api/logout', status, err.toString());
            Router.browserHistory.push('/');
        });
    }

    render() {
        return null
    }
}


class Register extends React.Component{
    constructor(props){
        super(props);
        this.state = {data: []};
        this.handleRegisterSubmit = this.handleRegisterSubmit.bind(this);
    }

    handleRegisterSubmit(user){
        $.ajax({
            method: 'POST',
            url: '/api/register',
            dataType: 'json',
            data: user,
            cache: false
        }).done((newUser) => {
            console.log('registered ' + newUser);
            alert("Successfull registration");
            Router.browserHistory.push('/');
        }).fail((xhr, status, err) => {
            console.error('/api/register', status, err.toString());
        });
    }

    render(){
        return (
            <div className="container content">
                <div className="row">
                    <RegisterForm onRegisterSubmit={this.handleRegisterSubmit} />
                </div>
            </div>
        );
    }
}

Login.contextTypes = {
    authService: React.PropTypes.object
};
Logout.contextTypes = {
    authService: React.PropTypes.object
};

export {Login, Logout, Register}
//export {Login, Logout }