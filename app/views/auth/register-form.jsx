import React from 'react';



class RegisterForm extends React.Component {
    constructor(props){
        super(props);
        this.state = {username: '', password: '', passwordRepeat: '', email: ''};
        this.setState = this.setState.bind(this);
        this.handleRegister = this.handleRegister.bind(this);
        this.handleUsername = this.handleUsername.bind(this);
        this.handlePassword = this.handlePassword.bind(this);
        this.handlePasswordRepeat = this.handlePasswordRepeat.bind(this);
        this.handleEmail = this.handleEmail.bind(this);
    }
    handleRegister(e){
        e.preventDefault();
        let username = this.state.username.trim();
        let password = this.state.password.trim();
        let passwordRepeat = this.state.passwordRepeat.trim();
        let email = this.state.email.trim();
        if(!username || !password || !passwordRepeat || !email) {
            alert('All inputs are required!');
            return;
        }
        if(username.length <=  3 || username.length > 20){
            alert("Username have to be between 4 and 20 chars");
            return;
        }
        if(password.length <= 5 ){
            alert("Password have to be more than 5 chars");
            return;
        }
        if(password.length > 30 ){
            alert("Password is > 30");
            return;
        }
        if(!this.validateEmail(email)){
            alert("Email is not valid!");
            return;
        }
        if (password !== passwordRepeat) {
            alert("Passwords dont match!");
            return;
        }

        this.props.onRegisterSubmit({username: username, password: password , passwordRepeat: passwordRepeat, email: email});
        this.setState({username: '', password: '', passwordRepeat: '', email: ''});
    }
    handleUsername(e){
        this.setState({username: e.target.value});
    }
    handlePassword(e){
        this.setState({password: e.target.value});
    }
    handlePasswordRepeat(e){
        this.setState({passwordRepeat: e.target.value});
    }

    handleEmail(e){
        this.setState({email: e.target.value});
    }

    validateEmail (email) {
    // regex from http://stackoverflow.com/questions/46155/validate-email-address-in-javascript
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    }

    render(){
        return (
            <div>
                <h2>Register</h2>
                <div>
                    <input type="text" value={this.state.username} placeholder="Username" onChange={this.handleUsername} />
                </div>
                <div>
                    <input type="password" value={this.state.password}  placeholder="Password" onChange={this.handlePassword} />
                </div>
                <div>
                    <input type="password" value={this.state.passwordRepeat}  placeholder="Repeat Password" onChange={this.handlePasswordRepeat} />
                </div>
                <div>
                    <input type="email" value={this.state.email}  placeholder="Email" onChange={this.handleEmail} />
                </div>
                <button onClick={this.handleRegister} >
                    Register
                </button>
            </div>
        );
    }
}

RegisterForm.propTypes = {
    onRegisterSubmit: React.PropTypes.func
};

export default RegisterForm;