import React from 'react';
import NavLink from '../../components/nav/nav-link';

class LoginForm extends React.Component{
    constructor(props){
        super(props);
        this.state = {username: '', password: ''};
        this.setState = this.setState.bind(this);
        this.handleusernameChange = this.handleusernameChange.bind(this);
        this.handleTextChange = this.handleTextChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }
    handleusernameChange(e){
        this.setState({username: e.target.value})
    }
    handleTextChange(e){
        this.setState({password: e.target.value})
    }
    handleSubmit(e){
        e.preventDefault();
        let username = this.state.username.trim();
        let password = this.state.password.trim();
        if(!password || !username){
            return;
        }
        this.props.onLoginSubmit({username: username, password: password});
        this.setState({username: '', password: ''})
    }
    render(){
        return (
            <form>
                <input type='username' value={this.state.username} placeholder='Username' onChange={this.handleusernameChange}/>
                <input type='password' value={this.state.password} placeholder='Password' onChange={this.handleTextChange}/>
                <input type='submit' value='Login' onClick={this.handleSubmit}/>
                <NavLink to="/register">Registration</NavLink>
            </form>
        );
    }
}

LoginForm.propTypes = {
    onLoginSubmit: React.PropTypes.func
};

export default LoginForm