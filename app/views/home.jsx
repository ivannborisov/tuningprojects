import React from 'react'
import LastProjects from '../components/projects/last-projects';
import LastActivities from '../components/last-activities';


class Home extends React.Component {
    constructor(props, context){
        super(props, context);
        this.context.titleService.setTitle('Tuning projects');

    }

    render() {
        return (
            <div>
                <div className="wrapper bg-gray">
                    <div className="container">
                        <div className="slider">

                        </div>
                    </div>
                </div>
                <div className="container content">
                    <div className="row">
                        <h4>Last projects</h4>
                        <LastProjects />
                    </div>
                    <div className="row">
                        <LastActivities/>
                    </div>
                </div>
            </div>

        );
    }

}

Home.contextTypes = {
    titleService: React.PropTypes.object
};

export default Home