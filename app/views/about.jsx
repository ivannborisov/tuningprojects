import React from 'react'

class Home extends React.Component {
    constructor(props, context){
        super(props,context);
        this.context.titleService.setTitle('About');
    }
    render() {
        return (

            <div className="container content">
                <div className="row">
                    <div>About</div>
                </div>
            </div>

        )
    }

}

Home.contextTypes = {
    titleService: React.PropTypes.object
};

export default Home