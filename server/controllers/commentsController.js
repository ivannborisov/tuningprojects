const Comment = require('mongoose').model('Comment');
const Photo = require('mongoose').model('Photo');
const Update = require('mongoose').model('Update');

const sockets = require('../utilities/socket');

module.exports = {

    createComment: function (req, res) {


        let newComment = {
            created: new Date(),
            content: req.body.content,
            user: req.body.user,
            toModel: req.body.toModel,
            toId: req.body.toId,
            project: req.body.project
        };

        if(req.body.toModel === 'post') {
            Update.findOne({_id: req.body.toId}).exec((err, post) => {
                Comment.create(newComment, (err, comment) => {
                    if(err){
                        console.log('Failed to create new Comment!'+ err);
                        return res.status(400).send('Not found');
                    }
                    sockets.newComment(comment);
                    post.comments.push(comment);
                    post.save();
                    res.json(comment);
                });
            });
        }
        else if(req.body.toModel === 'photo') {
            Photo.findOne({_id: req.body.toId}).exec((err, photo) => {
                Comment.create(newComment, (err, comment) => {
                    if(err){
                        console.log('Failed to create new Comment!'+ err);
                        return res.status(400).send('Not found');
                    }

                    sockets.newComment(comment);
                    photo.comments.push(comment);
                    photo.save();
                    res.json(comment);
                });
            });
        }
        else {
            res.status(400).send('Not found');
        }

    },

    getAllComments: function (req, res){

        Comment.find().exec((err, comments) => {
            if(err) {
                console.log('Projects could not be loaded' + err);
                return res.status(400).send('Not found');
            }
            return res.status(200).json(comments);
        });

    }
};