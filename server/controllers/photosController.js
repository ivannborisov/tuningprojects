const Photo = require('mongoose').model('Photo');
const Project = require('mongoose').model('Project');
const sockets = require('../utilities/socket');

module.exports = {

  uploadPhotos: function (req, res){
      let projectId = req.body.projectid;
      Project.findOne({_id:  projectId}).exec(function(err, project){

          let photos = [];
          for (let photo of req.files) {

              let newPhoto;
              newPhoto = {
                  name: photo.filename,
                  created: new Date(),
                  project: projectId
              };

              photos.push(newPhoto);

          }
          Photo.create(photos, (err, newPhotos) =>{
              if(err){
                  console.log('Failed to create new photo details!'+ err);
                  res.status(400);
                  res.send('Not found');
              }
              for(let ph of newPhotos )
                project.photos.push(ph);
              project.save();




              sockets.newPhotos(newPhotos);

              res.json({result: 'success'});

          });


      });



  }
};