const LastActivity = require('mongoose').model('LastActivity');
const sockets = require('../utilities/socket');

module.exports = {

    createPhotosActivity: function (photos) {
        let photosLength = photos.length;
        let newActivity = {
            content:photosLength == 1 ? 'User add photo in project' : `User add ${photosLength} photos in project`,
            type: 'photos',
            reference: photos[0].project,
            created: photos[0].created
        };

        LastActivity.create(newActivity, (err, activity) => {
            if(err) {
                console.log('Failed to create new activity!'+ err);
            }
            sockets.newActivity(activity);
        });
    },

    createCommentActivity: function(comment) {
        let newActivity = {
            content: 'User comment project',
            type: 'comment',
            reference: comment.project,
            created: comment.created

        };

        LastActivity.create(newActivity, (err, activity) => {
            if(err) {
                console.log('Failed to create new activity!'+ err);
            }
            sockets.newActivity(activity);
        });
    },

    createPostActivity: function (post) {
        let newActivity = {
            content: 'User update project',
            type: 'post',
            reference: post.project,
            created: post.created
        };

        LastActivity.create(newActivity, (err, activity) => {
            if(err) {
                console.log('Failed to create new activity!'+ err);
            }
            sockets.newActivity(activity);
        });
    },

    getLastActivities: function (req, res) {

        let filter = {};
        if(req.query.section) {
            filter = {
                section: req.query.section
            };
        }
        LastActivity.find(filter).populate('project user').sort({created: 'desc'}).exec(function (error, lastActivities) {
            if (error) {
                console.log('LastActivity could not be loaded' + err);
                res.status(404).send('Not found');
            }
            res.send(lastActivities);
        });
    },
};