const usersController = require('./usersController');
const projectsController = require('./projectsController');
const projectUpdatesController = require('./projectUpdatesController');
const photosController = require('./photosController');
const lastActivities = require('./lastActivitiesController');
const comments = require('./commentsController');
const socket = require('./../utilities/socket');

module.exports = {
  users : usersController,
  projects : projectsController,
  photos : photosController,
  updates : projectUpdatesController,
  lastActivities : lastActivities,
  comments: comments
};