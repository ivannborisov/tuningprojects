const Project = require('mongoose').model('Project');
const ProjectUpdate = require('mongoose').model('Update');
const sort = require('../utilities/sort');

function generateUpdates (project) {
    let updatesArr = project.updates;
    let photosArr = project.photos;
    // console.log('HERE');
    // console.log(photosArr[0].created.toString().substr(0,10));
   // console.log(project);

    let updatesGroupedByDate = {};
    for(let photo of photosArr) {

        let photoCreateDate = photo.created.toString().substr(0,10);

        if(updatesGroupedByDate[photoCreateDate])
            updatesGroupedByDate[photoCreateDate].photos.push(photo);

        else {
            updatesGroupedByDate[photoCreateDate] = {
                photos : [],
                posts : []
            };
            updatesGroupedByDate[photoCreateDate].photos.push(photo);

        }
    }

    for(let update of updatesArr) {

        let updateCreateDate = update.created.toString().substr(0,10);

        if(updatesGroupedByDate[updateCreateDate])
            updatesGroupedByDate[updateCreateDate].posts.push(update);
        else {
            updatesGroupedByDate[updateCreateDate] = {
                photos:[],
                posts: []
            };
            updatesGroupedByDate[updateCreateDate].posts.push(update);
        }
    }

    let updatesKeys =  Object.keys(updatesGroupedByDate);

    updatesKeys.sort((a, b) => {
        let date1 = new Date(a);
        let date2 = new Date(b);

        if (date1.getTime() > date2.getTime())
            return 1;
        if (date1.getTime() < date2.getTime())
            return -1;

        return 0;
    });



    let updatesSorted = [];
    for(let key of updatesKeys) {

        updatesGroupedByDate[key].photos.sort(sort.sortByDate);
        updatesGroupedByDate[key].posts.sort(sort.sortByDate);
        let update = {
            date: key,
            photos: updatesGroupedByDate[key].photos,
            posts: updatesGroupedByDate[key].posts
        };

        updatesSorted.unshift(update);
    }




    return updatesSorted;
}


module.exports = {
    getAllProjects: function (req, res) {

        let filter = {};
        if(req.query.section) {
            filter = {
                section: req.query.section
            };
        }

        Project.find(filter).exec(function (error, projects) {
            if (error) {
                console.log('Projects could not be loaded' + err);
                return res.status(400).send('Not found');
            }
            res.send(projects);
        });
    },

    getProjectById: function(req, res){
        Project.findOne({_id:  req.params.id})
            .populate({
                path: 'photos',
                populate: {
                    path: 'comments'
                }
            })
            .populate({
                path: 'updates',
                populate: {
                    path: 'comments'
                }
            })
            //.populate('photos updates')
            .exec(function(err, project){
            if(err) {
                console.log('Course could not be loaded: ' + err);
                res.status(400);
                res.send('Not found');
            }

            let updates = generateUpdates(project);


            let resProject = {
                _id : project._id,
                owner: project.owner,
                title: project.title,
                section: project.section,
                created: project.created,
                mainImg: project.owner,
                updates: updates
            };

            res.send(resProject);
            //res.send(project);
        });
    },






    createProject: function(req, res) {

        let newProjectData = req.body;
        newProjectData.created = new Date();
        newProjectData.tags = ['visual tuning', 'over 300HP']; // TO DO


        if(req.file) {
            console.log('After');
            console.log(req.file);
            newProjectData.mainImg = req.file.filename;
        }
        else {
            newProjectData.mainImg = 'default.jpg';
        }

        Project.create(newProjectData, function(err, project){
            if(err){
                console.log('Failed to create new project!'+ err);
                res.status(404);
                res.send('Not found');
            }

            res.send(project);


        });
    },
    updateProject: function(req, res) {
        let newProjectData = req.body;

        Project.findById( newProjectData.id , function(err, project){
            if(err){
                console.log('Failed to update project!'+ err);
                res.status(404);
                res.send('Not found');
            }
            Object.assign(project, newProjectData);
            project.save();
            res.send(project);

        })

    },
    deleteProject: function (req, res) {

        Project.remove({_id:  req.body.id}, function (err) {
            if(err){
                console.log('Failed to delete project!'+ err);
                res.status(404);
                res.send('Not found');
            }
            res.send({success: true});
        });
    }



};