const ProjectUpdate = require('mongoose').model('Update');
const Project = require('mongoose').model('Project');
const sockets = require('../utilities/socket');

module.exports = {
    createUpdate: function (req, res) {
        // var tomorrow = new Date();
        // tomorrow.setDate(tomorrow.getDate() + 1);

        let projectId = req.body.projectid;
        let newUpdate = {
            content: req.body.content,
            project: projectId,
          //  created: tomorrow
            created: new Date()
        };

        Project.findOne({_id:  projectId}).exec((err, project) => {
            ProjectUpdate.create(newUpdate, (err, update) => {
                if(err){
                   console.log('Failed to create new update!'+ err);
                   res.status(400);
                   res.send('Not found');
                }
                sockets.newPost(update);
                project.updates.push(update);
                project.save();
                res.json(update);
            });

        });
    },

    getProjectUpdates: function (req, res) {
        res.json({success: true});
    }

};