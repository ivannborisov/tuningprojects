const encryption = require('../utilities/encryption'),
      User = require('mongoose').model('User'),
      mailer = require('../config/mailer');


function validateEmail (email) {
    // regex from http://stackoverflow.com/questions/46155/validate-email-address-in-javascript
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}


module.exports = {

    createUser: function (req, res){
        let newUserData = req.body;
        newUserData.salt = encryption.generateSalt();
        newUserData.hashPass = encryption.generateHashedPassword(newUserData.salt, newUserData.password);
        newUserData.verificationEmailCode = encryption.generateRandomString();

        newUserData.username = newUserData.username.trim();
        newUserData.email = newUserData.email.trim();
        newUserData.password = newUserData.password.trim();

        let usernameLenght = newUserData.username.length;
        if(usernameLenght <= 3 || usernameLenght > 35)
            return  res.status(400).send({success: false, reason: 'username_length'});

        let passwordLenght = newUserData.password.length;
        if(passwordLenght <= 3 || passwordLenght > 35)
            return  res.status(400).send({success: false, reason: 'password_length'});

        if(!validateEmail(newUserData.email))
            return  res.status(400).send({success: false, reason: 'email_invalid'});

        if(newUserData.password !== newUserData.passwordRepeat)
            return  res.status(400).send({success: false, reason: 'password_notmatched'});

        User.findOne({username: newUserData.username}, (err, user) => {
            if(err)
                return  res.status(400).send({success: false, reason: 'bad_request'});
            console.log(user);
            if(user)
                return res.status(400).send({success: false, reason: 'username_exist'});

            User.findOne({email: newUserData.email}, (err, user) => {
                if(err)
                    return  res.status(400).send({success: false, reason: 'bad_request'});
                if(user)
                    return  res.status(400).send({success: false, reason: 'email_exist'});


                User.create(newUserData, (err,user) => {
                    if(err){
                        res.status(400);
                        res.send({success: false, reason: 'bad_request'});
                    }
                    else {
                        let fullUrl = req.protocol + '://' + req.get('host')  + '/accounts/activate/' + newUserData.verificationEmailCode;
                        message = {
                            from:'"TuningProjects-bg.com" <admin@support.tuningprojects-bg.com>' ,
                            to: newUserData.email,
                            subject: 'Activate your account',
                            html: `<h1>Thank you for registration.</h1><b>Please click on link below. </b><p><a href=${fullUrl}>${fullUrl}</a> </p>`
                        };

                        mailer.sendMail(message);
                        res.send({success: true});
                    }
                });
            });

        });
    },

    verifyEmail: function (req, res) {
        let verCode = req.params.verificationcode;
        console.log(req.params.verificationcode);
        User.findOneAndUpdate({verificationEmailCode: verCode}, { verifiedEmail: true, verificationEmailCode: ''} ,  (err, result) => {
            if(err)
               return res.status(400).end();
            res.send({success: true});
        });
    },

    sendVerificationCode: function(req, res) {
        let email = req.body.email;

        let verificationCode = encryption.generateRandomString();
        let fullUrl = req.protocol + '://' + req.get('host')  + '/accounts/activate/' + verificationCode;

        message = {
            from: '"TuningProjects-bg.com" <admin@support.tuningprojects-bg.com>' ,
            to: email,
            subject: 'Activate your account',
            html: `<b>Please click on link below. </b><p><a href=${fullUrl}>${fullUrl}</a> </p>`
        };

        mailer.sendMail(message);

        User.findOneAndUpdate({verificationEmailCode: email}, {  verificationEmailCode: verificationCode} ,  (err, result) => {
            if(err)
                return res.status(400).end();

            res.send({success: true});
        });



    }

};