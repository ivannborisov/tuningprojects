const Comment = require('mongoose').model('Comment');
const Photo = require('mongoose').model('Photo');
const Update = require('mongoose').model('Update');
const sort = require('../utilities/sort');

module.exports = {


    getLastActivities: function (req, res) {


        Comment.find({}).populate('project user').sort({created: 'desc'}).limit(7).exec(function (error, lastComments) {
            if (error) {
                console.log('Last comments could not be loaded' + err);
                return res.status(400).send('Bad request.');
            }
            lastComments = lastComments.map((obj) => {
                //console.log(Object.assign(obj, {type: 'comment'}));

                // let a = Object.assign({}, obj, {type: 'comment'});
                // a.type = 'comment';

                let activity = {
                    type: 'comment',
                    id: obj._id,
                    content: obj.content,
                    user: obj.user,
                    toModel: obj.toModel,
                    toId: obj.toId,
                    project: obj.project,
                    created: obj.created
                };

                return activity;
            });

            Photo.find({}).populate('project').sort({created: 'desc'}).limit(7).exec(function (error, lastPhotos) {
                if (error) {
                    console.log('Last photos could not be loaded' + err);
                    return res.status(400).send('Bad request.');
                }
                lastPhotos = lastPhotos.map((obj) => {

                    let activity = {
                        type: 'photo',
                        id: obj._id,
                        name: obj.name,
                        created: obj.created,
                        project: obj.project
                    };
                    return activity;
                });
                Update.find({}).populate('project').sort({created: 'desc'}).limit(7).exec(function (error, lastPosts) {
                    if (error) {
                        console.log('Last posts could not be loaded' + err);
                        return res.status(400).send('Bad request.');
                    }
                    lastPosts = lastPosts.map((obj) => {
                        let activity = {
                            type: 'post',
                            id: obj._id,
                            content: obj.content,
                            project: obj.project,
                            created: obj.created
                        };

                        return activity;
                    });

                    let lastActivities = lastComments.concat(lastPosts, lastPhotos);
                  //  console.log(lastActivities);
                    lastActivities.sort(sort.sortByDate);

                    res.status(200).json(lastActivities);

                });
            });

        });
    }

};