const express = require('express');
const app = express();
const cookieParser = require('cookie-parser');
const http = require('http');


const env = process.env.NODE_ENV || 'development';

const config = require('./config/config')[env];

app.use(cookieParser());
require('./config/express')(app, config);
require('./config/mongoose')(config);
require('./config/routes')(app);

const server = http.createServer(app).listen(config.port);
console.log(`Server listening on port ` + config.port);

let io = require('./config/io');
io.attach(server);