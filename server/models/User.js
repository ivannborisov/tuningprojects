const mongoose = require('mongoose'),
      encryption = require('../utilities/encryption');

var userSchema = mongoose.Schema({
    username: {type:String, require: '{PATH} is required', unique: true},
    firstName: {type:String, require: '{PATH} is required'},
    lastName: {type:String, require: '{PATH} is required'},
    email: {type: String, require: '{PATH} is required', unique: true},
    salt: String,
    hashPass: String,
    verifiedEmail: {type:Boolean, default: false},
    verificationEmailCode: String,
    roles: [String]
});

userSchema.method({
    authenticate: function (password){

        if(encryption.generateHashedPassword(this.salt, password)=== this.hashPass){
            return true;
        }
    }
});

var User = mongoose.model('User', userSchema);


module.exports.seedInitialUsers = function () {

   //User.remove({},function(){
        console.log('deleted');
        User.find({}).exec(function (err, collection) {
            if (err) {
                console.log(err);
                return;
            }

            if (collection.length === 0) {
                var salt;
                var hashedPwd;

                salt = encryption.generateSalt();
                hashedPwd = encryption.generateHashedPassword(salt, '123456');
                User.create({username: 'vanko', firstName: 'Ivan', lastName: 'Borisov',email: 'ivan@abv.bg', salt:salt,hashPass: hashedPwd , roles:['admin']});
                salt = encryption.generateSalt();
                hashedPwd = encryption.generateHashedPassword(salt, '123456');
                User.create({username: 'peshko', firstName: 'Pesho', lastName: 'Addeer',email: 'ivan@3abv.bg', salt:salt,hashPass: hashedPwd, roles:['standard']});
                salt = encryption.generateSalt();
                hashedPwd = encryption.generateHashedPassword(salt, '123456');
                User.create({username: 'goshku', firstName: 'Goshu', lastName: 'Eaadd',email: 'ivan@2abv.bg', salt:salt,hashPass: hashedPwd, roles:[]});
                console.log('Users are added to database');
            }

        });


        User.find({}).exec(function (err, users){
          // console.log(users);
        });
    //});
};