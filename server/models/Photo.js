const mongoose = require('mongoose');

let photoSchema = mongoose.Schema({
    name: String,
    created: Date,
    project: { type: mongoose.Schema.Types.ObjectId, ref: 'Project' },
    comments: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Comment' }],
});


let Photo = mongoose.model('Photo', photoSchema);

//Photo.remove({}, function () {});
Photo.find({}).exec(function(err, photos){
   // console.log(photos);
});

module.exports.seedInitialPhotos = function () {

    Photo.remove({}, function () {});
};