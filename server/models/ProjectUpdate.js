const mongoose = require('mongoose');

let projectUpdateSchema = mongoose.Schema({
    content: String,
    created: Date,
    project: { type: mongoose.Schema.Types.ObjectId, ref: 'Project' },
    comments: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Comment' }]
});

let ProjectUpdate = mongoose.model('Update', projectUpdateSchema);

ProjectUpdate.remove({project : '583c1bc0cb774610f663defb'}, function () {console.log('deleted')});
module.exports.seedInitialUpdates = function () {

    ProjectUpdate.remove({}, function () {});
};