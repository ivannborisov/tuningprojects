const mongoose = require('mongoose');

let commentSchema = mongoose.Schema({
    created: Date,
    content: String,
    user: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
    toModel: mongoose.Schema.Types.String,
    toId: mongoose.Schema.Types.ObjectId,
    project: {type: mongoose.Schema.Types.ObjectId, ref: 'Project'}
});

let Comment = mongoose.model('Comment', commentSchema);