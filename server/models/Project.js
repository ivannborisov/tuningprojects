const mongoose = require('mongoose');

let projectSchema = mongoose.Schema({
    title: String,
    owner: String,
    created: Date,
    section: String,
    tags: [String],
    photos: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Photo' }],
    updates: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Update' }],
    mainImg: String
});

let Project = mongoose.model('Project', projectSchema);
//Project.remove({_id: '5821ab62bedaea0fdda3f571'},function() {});
module.exports.seedInitialProjects = function () {

    Project.remove({},function() {});

  // Project.remove({},function() {
        Project.find({}).exec(function (err, collection) {
            if (err) {
                console.log(err);
                return;
            }

            if (collection.length === 0) {
                // Project.create({
                //     title: 'Audi S7',
                //     owner: 'vanko',
                //     section: 'cars',
                //     created: new Date('10/12/2015'),
                //     tags: ['engine', '300HP'],
                //     mainImg: 'audi1.jpg'
                // });
            }
            else {
                //console.log(collection);
            }
        });
  //  });
};