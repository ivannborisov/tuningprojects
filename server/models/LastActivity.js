const mongoose = require('mongoose');

var lastActivitySchema = mongoose.Schema({
    content: String,
    created: Date,
    type: String,
    reference: String,
    project: { type: mongoose.Schema.Types.ObjectId, ref: 'Project' },
    user: { type: mongoose.Schema.Types.ObjectId, ref: 'User' }
});

var LastActivity = mongoose.model('LastActivity', lastActivitySchema);

module.exports.getAll = function () {

    LastActivity.find({}).exec(function (err, collection) {

       console.log(collection);
    });

};