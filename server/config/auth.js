
const jwt = require('jsonwebtoken');
const User = require('mongoose').model('User');


function decodeToken(token, cb) {

    let decode = jwt.verify(token, 'bigSecret123456');

    if(decode)
        return decode._doc;
    return undefined;

}



module.exports = {

    login: function (req, res, next) {

        let username = req.body.username;
        let password = req.body.password;
        if(username && password) {
            User.findOne({username: username}).exec(function (err, user) {
                if (err) {
                    return res.status(401).json({success: false, message: 'Invalid username or password!'});
                }

                if (user && user.authenticate(password)) {

                    if(!user.verifiedEmail){
                        return res.status(401).json({success: false, reason: 'email_not_verified' , message: 'Your email is not verified!'});
                    }

                    var token = jwt.sign(user, 'bigSecret123456', {
                        expiresIn : 60*60*24 // expires in 24 hours
                    });

                    return res.cookie('jwt', token, {expire : new Date() + 9999}).json({success:true, user:user}); // TO DO - send part of user information
                }
                else {
                    return res.status(401).json({success: false, message: 'Invalid username or password!'});
                }
            });
        }
        else
            res.json({success: false});
    },
    logout: function (req, res, next) {
        res.clearCookie('jwt');
        res.end();
    },
    isAuthenticated: function(req,res,next){
        if (true) { // TO DO
            res.status(403).end();
        }
        else {
            next();
        }

    },
    isInRole: function (role){  // TO DO
        return function(req,res,next){
            if(req.isAuthenticated() && req.user.roles.indexOf(role)> -1 ){
                next();
            }
            else {
                res.status(403).end();

            }

        }
    },
    checkAuth: function (req, res, next) {
        // check header or url parameters or post parameters for token
        var token =req.cookies.jwt;

        // decode token
        if (token) {

            let user = decodeToken(token);
            if (user) {
                next();
            }
            else {
                return res.status(403).json({
                    success: false,
                    message: 'Forbidden 403'
                });
            }

        } else {
            return res.status(403).json({
                success: false,
                message: 'Forbidden 403'
            });

        }
    },
    checkAuthReturnUser: function(req, res){
        var token =req.cookies.jwt;

        // decode token
        if (token) {

            let user = decodeToken(token);
            if (user) {
                return res.json({success: true, user: user});
            }
            else {
                return res.json({success: false});
            }

        } else
            return res.json({success: false});

    },
    getCurrentUser: function(req, res) {
        var token =req.cookies.jwt;

        if (token) {

            return decodeToken(token);
        }
        else
            return undefined;
    }
};