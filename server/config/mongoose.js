var mongoose = require('mongoose'),
    user = require('../models/User'),
    projects = require('../models/Project'),
    projectUpdate = require('../models/ProjectUpdate'),
    lastActivity = require('../models/LastActivity'),
    comments = require('../models/Comment'),
    photos = require('../models/Photo');

module.exports = function (config) {
    mongoose.Promise = global.Promise;
    mongoose.connect(config.db);

    var db = mongoose.connection;

    db.once('open', function (err){
        if(err){
           console.log('Database could not be open' + err);
           return;
        }

        console.log('Database up and running');
    });

    db.on('error',function (err){
        console.log('Database error' + err);
    });

    user.seedInitialUsers();
    //projects.seedInitialProjects();
    //photos.seedInitialPhotos();
    //projectUpdate.seedInitialUpdates();
};