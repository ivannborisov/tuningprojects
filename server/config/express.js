const express = require('express');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const session = require('express-session');

module.exports = function (app, config){

    app.use(cookieParser());
    app.use(bodyParser.urlencoded({ extended: true }));
    app.use(bodyParser.json());
    app.use(session({'secret': 'Iborisov987456321159753', resave: true, saveUninitialized: true})); //, cookie: {maxAge: 60000}
    app.use(express.static(config.rootPath + '' + '/public'));

    app.use(function (req,res,next){
        
        //console.log("Middleware");
        next();
    });

};