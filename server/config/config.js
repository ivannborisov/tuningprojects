const path = require('path');
const rootPath = path.normalize(__dirname + '/../../')

module.exports = {
    development: {
        rootPath: rootPath,
        db: 'mongodb://172.17.0.2:27017/tuningprojects',
        port: process.env.PORT || 9000
    },
    production:{

        rootPath: rootPath,
        db: '',
        port: process.env.PORT || 9001
    }

};