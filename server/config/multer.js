const  multer  = require('multer'),
       crypto = require('crypto');

let storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, './public/images/uploads' )
    },
    filename: function (req, file, cb) {
        crypto.pseudoRandomBytes(16, function (err, raw) {

            let fileName = raw.toString('hex') + Date.now() + '.' + file.originalname.split('.')[1];
            cb(null, fileName);
        });
    }
});

let upload = multer({ storage: storage });

module.exports = {
    upload
};