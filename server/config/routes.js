const auth = require('./auth'),
      controllers = require('../controllers'),
      multer  = require('./multer');



module.exports = function (app){

    app.get('/', function (req, res) {
        console.log(req.protocol);
        console.log(req.get('host'));
        console.log(req.protocol + '://' + req.get('host') + req.originalUrl);
        res.send('Hello World!');
    });

    app.get('/api/projects', controllers.projects.getAllProjects);
    app.get('/api/projects/:id', controllers.projects.getProjectById);
    app.post('/api/projects',auth.checkAuth, multer.upload.single('photo'), controllers.projects.createProject);
    app.put('/api/projects', controllers.projects.updateProject);
    app.delete('/api/projects', controllers.projects.deleteProject);

    app.post('/api/uploadphotos', multer.upload.array('photos', 10) ,controllers.photos.uploadPhotos);

    app.post('/api/checklogged', auth.checkAuthReturnUser);

    app.post('/api/updates', auth.checkAuth, controllers.updates.createUpdate);
    app.get('/api/updates', auth.checkAuth, controllers.updates.getProjectUpdates);

    app.post('/api/comments', auth.checkAuth, controllers.comments.createComment);
    app.get('/api/comments', controllers.comments.getAllComments);

    app.get('/api/lastactivities', controllers.lastActivities.getLastActivities);

    app.post('/api/register', controllers.users.createUser);
    app.get('/api/verifyemail/:verificationcode', controllers.users.verifyEmail);
    app.post('/api/verifyemail', controllers.users.sendVerificationCode);
    app.get('/api/secure', auth.checkAuth, function (req, res) {
       res.send('Secure page');
    });
    app.post('/api/login', auth.login);
    app.get('/api/logout', auth.logout);

    app.get('/api/emitsocket', (req, res) => {
        res.send({});
    });
};