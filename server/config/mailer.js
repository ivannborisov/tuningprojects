var nodemailer = require('nodemailer');


var sendMail = function (message) {

    var transporter;

    transporter = nodemailer.createTransport({
        service: 'Mailgun',
        auth: {
            user:'admin@support.tuningprojects-bg.com',
            pass: '123456'
        }
    });

    transporter.sendMail(message, function(error, info){
        if (error) {
            console.log(error);
        }
        else {
            console.log('Sent: ' + info.response);
        }
    });
};


module.exports = {

  sendMail: sendMail

};
