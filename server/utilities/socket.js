const io = require('../config/io');

module.exports = {


    newComment: function (comment) {
        let commentObj = comment;
        commentObj.id = comment._id;
        delete commentObj._id;
        io.sockets.emit('newcomment', {comment: commentObj});
    },

    newPost: function (post) {
        let createdDate = post.created.toString().substr(0,10);
        io.sockets.emit('newpost', {date:createdDate, post:post});
    },

    newPhotos: function (photos) {
        let createdDate = photos[0].created.toString().substr(0,10);
        io.sockets.emit('newphotos', {date:createdDate, photos:photos, projectId: photos[0].project});
    },

    newActivity: function (activity) {
        io.sockets.emit('newactivity', {activity: activity});
    }


};
