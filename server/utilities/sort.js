module.exports = {
    sortByDate: function sortFunc (a, b) {
        if(a.created.getTime() > b.created.getTime())
            return -1;
        if(a.created.getTime() < b.created.getTime())
            return 1;
        return 0;
    }
};